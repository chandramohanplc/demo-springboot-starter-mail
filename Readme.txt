This source code demonstrates how to use the Spring Boot - springboot-start-mail component

1. You will need to update the following properties found in src/main/resource/application.properties

    spring.mail.username=
    spring.mail.password=

    send.to.email=
    send.from.email=

2. after setting your custom properties you can run the following from comamnd line using the gradle wrapper
(gradle wrapper allows you to run gradle without having it installed and configured on your system)

./gradlew bootRun